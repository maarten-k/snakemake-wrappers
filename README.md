[![Snakemake](https://img.shields.io/badge/snakemake-≥3.8.0-brightgreen.svg)](https://bitbucket.org/johanneskoester/snakemake)
[![CircleCI](https://circleci.com/bb/snakemake/snakemake-wrappers/tree/master.svg?style=shield)](https://circleci.com/bb/snakemake/snakemake-wrappers/tree/master)


# The Snakemake Wrapper Repository

The Snakemake Wrapper Repository is a collection of reusable wrappers that allow to quickly use popular command line tools
from [Snakemake](https://snakemake.readthedocs.io) rules and workflows.

Visit https://snakemake-wrappers.readthedocs.org for more information.
